package com.starza4tw.picturebook;

import com.google.inject.Inject;
import com.starza4tw.picturebook.commands.AddCommand;
import com.starza4tw.picturebook.commands.FormatCommand;
import com.starza4tw.picturebook.commands.ListCommand;
import com.starza4tw.picturebook.commands.ModuleCommand;
import com.starza4tw.picturebook.commands.RemoveCommand;
import com.starza4tw.picturebook.filter.CommandListener;
import com.starza4tw.picturebook.filter.FilteredMessageChannel;
import com.starza4tw.picturebook.filter.SignListener;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameConstructionEvent;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.service.pagination.PaginationService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.channel.MessageChannel;
import org.spongepowered.api.text.format.TextColors;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@Plugin(id = "picturebook", name = "Picturebook", version = "#version", description = "Picturebook is a Sponge plugin that replaces variables within the game with defined replacements.", authors = "Starza4TW") public class Main 
{
    @Inject @DefaultConfig(sharedRoot = true) Path configurationDirectory;

    @Inject public Logger logger;

    @Inject public PluginContainer pluginContainer;

    public FilteredMessageChannel filterChannel = new FilteredMessageChannel();

    public PaginationService paginationService;
    
    private static Main mainInstance;

    private Map<String, String> formatModeMap = new HashMap<String, String>();
    
    private Map<String, String> moduleMap = new HashMap<String, String>(); 

    @Listener public void onConstruct(GameConstructionEvent event) 
    {
        mainInstance = this;
        formatModeMap.put("item", "item");
        moduleMap.put("command", "command");
        moduleMap.put("chat", "chat");
        moduleMap.put("item", "item");
        moduleMap.put("sign", "sign");
    } 

    @Listener public void onInitialization(GameInitializationEvent event)
    {
        ConfigurationManager.initializeConfiguration(configurationDirectory);
        paginationService = Sponge.getServiceManager().provide(PaginationService.class).get();
        Sponge.getEventManager().registerListeners(this, new CommandListener());
        Sponge.getEventManager().registerListeners(this, new SignListener());
        Sponge.getEventManager().registerListeners(this, new ReloadListener());
    }

    @Listener public void onStarting(GameStartingServerEvent event) 
    {
        ConfigurationManager.loadConfiguration();
        Sponge.getCommandManager().register(this, CommandSpec.builder()
                .description(Text.of("Base command for Picturebook."))
                .extendedDescription(Text.of(
                        Text.of(TextColors.GOLD, "Picturebook Child Commands:\n"),
                        Text.of(TextColors.BLUE, "/pb add [key] [value] "), "Adds new entries to the filter.\n",
                        Text.of(TextColors.BLUE, "/pb format [item] "), "Temporary command that filters item names.\n",
                        Text.of(TextColors.BLUE, "/pb list "), "Lists all entries to the filter.\n", 
                        Text.of(TextColors.BLUE, "/pb module [module] [bool] "), "Globally enables/disables modules.\n",
                        Text.of(TextColors.BLUE, "/pb remove [key] "), "Removes entries from the filter."))
                .child(CommandSpec.builder()
                        .executor(new AddCommand())
                        .arguments(
                                GenericArguments.string(Text.of("key")), 
                                GenericArguments.string(Text.of("value")))
                        .permission("picturebook.command.add")
                        .description(Text.of("Adds new entries to the filter."))
                        .build(), "add", "create", "a")
                .child(CommandSpec.builder()
                        .executor(new FormatCommand())
                        .arguments(
                                GenericArguments.choices(Text.of("formatMode"), formatModeMap))
                        .description(Text.of("Temporary command that filters item names."))
                        .build(), "format", "f")
                .child(CommandSpec.builder()
                        .executor(new ListCommand())
                        .permission("picturebook.command.list")
                        .description(Text.of("Lists all entries to the filter."))
                        .build(), "list" , "l")
                .child(CommandSpec.builder()
                        .executor(new ModuleCommand())
                        .arguments(
                                GenericArguments.choices(Text.of("module"), moduleMap),
                                GenericArguments.bool(Text.of("bool")))
                        .permission("picturebook.command.module")
                        .description(Text.of("Globally enables/disables modules."))
                        .build(), "module", "m")
                .child(CommandSpec.builder()
                        .executor(new RemoveCommand())
                        .arguments(
                                GenericArguments.string(Text.of("key")))
                        .permission("picturebook.command.remove")
                        .description(Text.of("Removes entries from the filter."))
                        .build(), "remove", "delete", "r")
                .build(), "picturebook", "pb");
    }

    @Listener public void onChat(MessageChannelEvent.Chat event)
    {
        event.setChannel(MessageChannel.combined(event.getOriginalChannel(), filterChannel));
    }
    
    public static Main getInstance()
    {
        return mainInstance;
    }
}