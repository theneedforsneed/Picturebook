package com.starza4tw.picturebook.filter;

import com.starza4tw.picturebook.ConfigurationManager;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.util.regex.Pattern;

public class FilterUtilities 
{   
    public static final String filter(String string)
    {
        for(String Key : ConfigurationManager.FilterMap.keySet())
        {
            if(string.toLowerCase().contains(Key.toLowerCase()))
            {
                string = string.replaceAll("(?i)" + Pattern.quote(Key), ConfigurationManager.FilterMap.get(Key));
            }
        }
        return string;
    }
    
    public static Text format(String string)
    {
        return TextSerializers.FORMATTING_CODE.deserialize(string);
    }
    
    public static String simplify(Text text)
    {
        return TextSerializers.FORMATTING_CODE.serialize(text);
    }
}