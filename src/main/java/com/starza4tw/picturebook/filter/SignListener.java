package com.starza4tw.picturebook.filter;

import com.starza4tw.picturebook.ConfigurationManager;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.tileentity.ChangeSignEvent;
import org.spongepowered.api.event.filter.cause.First;

public class SignListener 
{
    @Listener public void onSignChange(ChangeSignEvent event, @First Player player)
    {
        if(player.hasPermission("picturebook.module.sign") && ConfigurationManager.useSignModule) 
        {
            for(int lineNumber = 0; lineNumber <= 3; lineNumber++)
            {
                event.getText().set(event.getText().getValue(Keys.SIGN_LINES).get().set(lineNumber, FilterUtilities.format(FilterUtilities.filter(event.getText().asList().get(lineNumber).toPlain()))));
            }
        }
    }
}