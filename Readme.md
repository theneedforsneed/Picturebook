Welcome to the Gitlab repository for Picturebook!

Picturebook is a chat/command/sign/item-name filter that replaces keywords with definable replacements. Use it to censor words, add in emoticons, or to make easy shortcuts for formatting codes.

Picturebook started out as a plugin for the Spigot API and has since been remade into a Sponge API plugin starting with the 2.0 series.
